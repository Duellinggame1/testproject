#!/bin/bash
#Variables
FILE='chess.pgn'
game_list=()
event=
site=
date=
round=
white=
black=
result=
whiteElo=
blackElo=
eco=
moves=
#The algo
if [ $# = 0 ]
 then
    echo "Please supply a url for posting the game data."
 else
  i=1
  #Reading each line
  while read line; do  
  #Create a JSON object based on the provided variables
  #Trigger the action every 13 lines
  if [ $(($i % 13)) -eq 0 ]
   then
   json=$(cat <<JSON
{
  "type": "Game",
  "fields": {
    "Event": "$event",
    "Site": "$site",
    "Date": "$date",
    "Round": "$round",
    "White": "$white",
    "Black": "$black",
    "Result": "$result",
    "WhiteElo": "$whiteElo",
    "BlackElo": "$blackElo",
    "ECO": "$eco"
  },
  "moves": "$moves"
}
JSON
)
game_list+=("$json")
fi
    #Read the incoming line and separate it into a tag and a value
    #Remove [ and ] characters from the input
    #Remove all hidden characters from the value
    #Assign the value to the corresponding variables based on the tag (except for the moves
    read tag value <<< $(echo $line | sed 's/\[//;s/\]//')
    value=$(echo "$value" | tr -d '[:cntrl:]' | sed 's/\\"/"/g; s/"//g')
    case "$tag" in
      "Event")
      event=$value
      ;;
      "Site")
      site=$value
      ;;
      "Date")
      date=$value
      ;;
      "Round")
      round=$value
      ;;
      "White")
      white=$value
      ;;
      "Black")
      black=$value
      ;;
      "Result")
      result=$value
      ;;
      "WhiteElo")
      whiteElo=$value
      ;;
      "BlackElo")
      blackElo=$value
      ;;
      "ECO")
      eco=$value
      ;;
      "1"*)
      moves=$(echo "$line" | tr -d '[:cntrl:]')
      ;;
    esac
    i=$((i+1))
    done < $FILE
fi
#Send POST requests to the backend
for game in "${game_list[@]}"
do
    curl -X POST -H "Content-Type: application/json" -d "$game" $1
done
#curl -X DELETE $1