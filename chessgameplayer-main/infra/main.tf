provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "devops_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true 

  tags = {
    Name = "Terraform_VPC"  
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.devops_vpc.id

  tags = {
    Name = "igw_terraform"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.devops_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Terraform_Route_Table"
  }
}

resource "aws_security_group" "frontend_security_group" {
  name_prefix = "frontend_security_group"
  description = "Allow SSH on port 22 and HTTP traffic on port 80"
  vpc_id = aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "lb_security_group" {
  name_prefix = "lb_security_group"
  description = "Allow SSH on port 22 and HTTP traffic on port 5000"
  vpc_id = aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "backend_security_group" {
  name_prefix = "backend_security_group"
  description = "Allow SSH on port 22 and HTTP traffic from loadbalancer"
  vpc_id = aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    security_groups = [aws_security_group.lb_security_group.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_subnet" "subnets" {
  for_each = var.subnets
  
  vpc_id = aws_vpc.devops_vpc.id
  cidr_block = each.value.cidr_block
  availability_zone = each.value.az
  tags = {
    Name = "${each.key}-subnet"
  }
}


resource "aws_route_table_association" "rta" {
  for_each = aws_subnet.subnets

  subnet_id      = each.value.id
  route_table_id = aws_route_table.route_table.id
}

resource "aws_lb_target_group" "target_group" {
  name     = "target-group"
  port     = 5000
  protocol = "HTTP"
  vpc_id   = aws_vpc.devops_vpc.id

  health_check {
    path     = "/"
    port     = "traffic-port"
    protocol = "HTTP"
  }
}


resource "aws_instance" "front_end_instance" {
  ami           = "ami-007855ac798b5175e"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.subnets["subnet-1"].id
  key_name      = "devops_key"
  user_data     = filebase64("${path.module}/front-init.sh")
  vpc_security_group_ids = [aws_security_group.frontend_security_group.id]
  tags = {
    Name = "front_end_instance"
  }
  associate_public_ip_address = true
}


#resource "aws_launch_configuration" "backend_launch_config" {
#  name_prefix = "Terraform_Launch_Config"
#  image_id = "ami-007855ac798b5175e"
#  instance_type = "t2.micro"
#  associate_public_ip_address = true
#  key_name = "devops_key"
#  user_data = filebase64("${path.module}/back-init.sh")
#  security_groups = [aws_security_group.backend_security_group.id]
#}
#
#
#resource "aws_autoscaling_group" "asg" {
#  name = "Terraform_ASG"
#  launch_configuration = aws_launch_configuration.backend_launch_config.name
#  min_size = 2
#  desired_capacity = 2
#  max_size = 4
#  vpc_zone_identifier = flatten([values(aws_subnet.subnets)[*].id])
#  target_group_arns = [aws_lb_target_group.target_group.arn]
#
#  tag {
#    key = "Name"
#    value = "Terraform_AS"
#    propagate_at_launch = true
#  }
#
#  lifecycle {
#    create_before_destroy = true
#  }
#
#  depends_on = [aws_launch_configuration.backend_launch_config]
#}
#
#resource "aws_lb" "load_balancer" {
#    name = "Backend-Loadbalancer"
#    internal = false
#    load_balancer_type = "application"
#    subnets = values(aws_subnet.subnets)[*].id
#    security_groups = [aws_security_group.lb_security_group.id]
#
#}
#
#resource "aws_lb_listener" "http" {
#  load_balancer_arn = aws_lb.load_balancer.arn
#  port              = 5000
#  protocol          = "HTTP"
#
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.target_group.arn
#  }
#}
#
#
#resource "aws_autoscaling_policy" "scale_policy" {
#  name                   = "cpu-utilization-scale-policy"
#  policy_type            = "TargetTrackingScaling"
#  autoscaling_group_name = aws_autoscaling_group.asg.name
#
#
#  target_tracking_configuration {
#    predefined_metric_specification {
#      predefined_metric_type = "ASGAverageCPUUtilization"
#    }
#     # Creates instances when CPU utilization goes above 70%
#    target_value = 70.0
#  }
#}
