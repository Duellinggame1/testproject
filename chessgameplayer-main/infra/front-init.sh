#!/bin/bash
apt-get update
apt-get install git vite npm docker-compose -y
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
source ~/.bashrc
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install v16.10.0
rm -rf /var/www/html/*
git clone https://gitlab.com/Duellinggame1/testproject
cd testproject/chessgameplayer-main
chmod 755 dockerize.sh
./dockerize.sh build
./dockerize.sh run