variable "subnets" {
  type = map(object({
    az  = string
    cidr_block = string
  }))
  default = {
    "subnet-1" = {
      az = "us-east-1a"
      cidr_block = "10.0.1.0/24"
    },
    "subnet-2" = {
      az = "us-east-1b"
      cidr_block = "10.0.2.0/24"
    }
  }
}