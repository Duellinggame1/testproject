#!/bin/bash
# 
# TODO: Implement the logic for the commands below. 
# 
# Usage:
#  build - Create docker container for the backend application
#  run   - Run the backend as a container (you can use port 5000 in your port mapping)
#  stop  - Stop the backend container

case $1 in

build)
cd frontend

npm install
npm i @mliebelt/pgn-viewer
npm run build

cd ..

docker-compose build
;;

run)

docker-compose up -d
;;

stop)
docker-compose down
;;

*)
echo "Command not found"
;;

esac